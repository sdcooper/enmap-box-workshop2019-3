.. _GDAL: https://www.gdal.org

.. _QtWidgets: https://doc.qt.io/qt-5/qtwidgets-index.html
.. _QtCore: https://doc.qt.io/qt-5/qtcore-index.html
.. _QtGui: https://doc.qt.io/qt-5/qtgui-index.html
.. _qgis.gui: https://qgis.org/api/group__gui.html
.. _qgis.core: https://qgis.org/api/group__core.html

.. Singular
.. _QApplication: https://doc.qt.io/qt-5/qapplication.html
.. _QAbstractScrollArea: https://doc.qt.io/qt-5/qabstractscrollarea.html

.. _QWidget: https://doc.qt.io/qt-5/qwidget.html
.. _QPushButton: https://doc.qt.io/qt-5/qpushbutton.html
.. _QToolButton: https://doc.qt.io/qt-5/qtoolbutton.html
.. _QTextEdit: https://doc.qt.io/qt-5/qtextedit.html
.. _QLabel: https://doc.qt.io/qt-5/qlabel.html
.. _QgsApplication: https://qgis.org/api/classQgsApplication.html
.. _QgsMapLayer: https://qgis.org/api/classQgsMapLayer.html
.. _QgsRasterLayer: https://qgis.org/api/classQgsRasterLayer.html
.. _QgsVectorLayer: https://qgis.org/api/classQgsVectorLayer.html
.. _QgsMapCanvas: https://qgis.org/api/classQgsMapCanvas.html
.. _QgsCoordinateReferenceSystem: https://qgis.org/api/classQgsCoordinateReferenceSystem.html
.. _QgsProject: https://qgis.org/api/classQgsProject.html
.. _QgsMapLayerStore: https://qgis.org/api/classQgsMapLayerStore.html

.. Plural
.. _QApplications: https://doc.qt.io/qt-5/qapplication.html
.. _QWidgets: https://doc.qt.io/qt-5/qwidget.html
.. _QPushButtons: https://doc.qt.io/qt-5/qpushbutton.html
.. _QToolButtons: https://doc.qt.io/qt-5/qtoolbutton.html
.. _QgsApplications: https://qgis.org/api/classQgsApplication.html
.. _QgsMapLayers: https://qgis.org/api/classQgsMapLayer.html
.. _QgsRasterLayers: https://qgis.org/api/classQgsRasterLayer.html
.. _QgsVectorLayers: https://qgis.org/api/classQgsVectorLayer.html
.. _QgsMapCanvass: https://qgis.org/api/classQgsMapCanvas.html
.. _QgsCoordinateReferenceSystems: https://qgis.org/api/classQgsCoordinateReferenceSystem.html
.. _QgsRasterDataProviders: https://qgis.org/api/classQgsRasterDataProvider.html